//
//  UnknownTests.m
//
//  Created by RASHAD on 5/30/15.
//

#import <XCTest/XCTest.h>

@interface UnknownTests : XCTestCase

@end

@implementation UnknownTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
