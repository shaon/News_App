//
//  AddNewsProvider.m
//
//  Created by RASHAD on 5/30/15.
//

#import "AddNewsProvider.h"
#import "AddLinksPhone.h"
#import "AddNewsProviderCellPad.h"

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface AddNewsProvider () {
    UITextField *nameA,*links;
}

@end

@implementation AddNewsProvider

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed: @"back"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    
    backButton.tintColor = [UIColor colorWithRed:70/255.f green:130/255.f blue:47/255.f alpha:1];
    
    self.navigationItem.leftBarButtonItem = backButton;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addNewLink {
    
    if (_rowNumber >= 0) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray *providerArray = [[NSMutableArray alloc] init];
        
        if ([userDefaults objectForKey:@"provider"] != NULL) {
            providerArray = [[userDefaults objectForKey:@"provider"] mutableCopy];
        }
        
        NSMutableDictionary *tempProvider = [[NSMutableDictionary alloc] init];
        [tempProvider setObject: links.text forKey:@"link"];
        [tempProvider setObject:nameA.text forKey:@"name"];
        
        [providerArray replaceObjectAtIndex:_rowNumber withObject:tempProvider];
        
        [userDefaults setObject:providerArray forKey:@"provider"];
        [userDefaults synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Link edited!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray *providerArray = [[NSMutableArray alloc] init];
        
        if ([userDefaults objectForKey:@"provider"] != NULL) {
            providerArray = [[userDefaults objectForKey:@"provider"] mutableCopy];
        }
        NSLog(@"%@",providerArray);
        NSMutableDictionary *tempProvider = [[NSMutableDictionary alloc] init];
        [tempProvider setObject: links.text forKey:@"link"];
        [tempProvider setObject:nameA.text forKey:@"name"];
        
        [providerArray addObject:tempProvider];
        
        [userDefaults setObject:providerArray forKey:@"provider"];
        [userDefaults synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Link added!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == nameA)
    {
        [links becomeFirstResponder];
    }
    else if (textField == links){
        [self addNewLink];
    }
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IPAD) {
        AddNewsProviderCellPad *cell = (AddNewsProviderCellPad *) [tableView dequeueReusableCellWithIdentifier:@"AddNewsProviderCellPad"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddNewsProviderCellPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.name.returnKeyType = UIReturnKeyNext;
        cell.links.returnKeyType = UIReturnKeyGo;
        cell.name.delegate = self;
        cell.links.delegate = self;
        
        
        cell.name.text = _name;
        cell.links.text = _url;
        nameA = cell.name;
        links = cell.links;
        [cell.addButton addTarget:self action:@selector(addNewLink) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else {
        AddLinksPhone *cell = (AddLinksPhone *) [tableView dequeueReusableCellWithIdentifier:@"AddLinksPhone"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddLinksPhone" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.name.returnKeyType = UIReturnKeyNext;
        cell.links.returnKeyType = UIReturnKeyGo;
        cell.name.delegate = self;
        cell.links.delegate = self;
        
        cell.name.text = _name;
        cell.links.text = _url;
        nameA = cell.name;
        links = cell.links;
        [cell.addButton addTarget:self action:@selector(addNewLink) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IPAD) {
        return 620.f;
    }
    return 320.f;
}

@end
