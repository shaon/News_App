//
//  LinkListController.h
//
//  Created by RASHAD on 5/30/15.
//

#import <UIKit/UIKit.h>

@interface LinkListController : UITableViewController<NSXMLParserDelegate>

@property (strong, nonatomic) NSString *urlString;
@property (strong, nonatomic) NSString *name;

@end
