//  Created by RASHAD on 5/30/15.
//

#import <UIKit/UIKit.h>
#import "GeneralService.h"

@interface LoginTableView : UITableViewController <UITextFieldDelegate>

@property(nonatomic,strong) GeneralService *generalService;

-(void)saveLoginDetails;
-(void)loadLoginDetails;

@end
