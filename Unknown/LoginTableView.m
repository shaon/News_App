//  Created by RASHAD on 5/30/15.
//

#import "LoginTableView.h"
#import "AppDelegate.h"
#import "LoginCellPad.h"
#import "LoginTableViewCell.h"
#import "RegistrationTable.h"
#import "NewsProviderList.h"

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface LoginTableView ()
{
    UITextField *emailText;
    UITextField *passwordText;
    
    NSString *userID, *password;
    
    BOOL rememberStatus;
    
    NSArray *userNameArray, *passwordArray;
}

@end

@implementation LoginTableView

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden:YES];
    
//    self.tableView.backgroundColor = [UIColor colorWithRed:180/255.f green:195/255.f blue:110/255.f alpha:1];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
//    userNameArray = [[NSArray alloc] initWithObjects:@"ataur", @"mahmud", @"amir", @"mizan", nil];
//    passwordArray = [[NSArray alloc] initWithObjects:@"ataur", @"mahmud", @"amir", @"mizan", nil];
    
//    [self loadLoginDetails];
    
    rememberStatus = true;
    //[self setComponent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard
{
    [self.view endEditing:YES];
}


- (void)loginButtonPressed:(id)sender
{
    self.generalService = [[GeneralService alloc] init];
    [self dismissKeyboard];
    
    [self performLogin];
}

-(void)registrationButtonPressed:(id)sender
{
    RegistrationTable *registration = [[RegistrationTable alloc] init];
    [self.navigationController pushViewController:registration animated:YES];
}


-(BOOL)loginAuthentication
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *userArray = [userDefaults objectForKey:@"users"];
    
    for (int i = 0 ; i < userArray.count; i++) {
        if ([[[userArray objectAtIndex:i] objectForKey:@"username"] isEqualToString:emailText.text]) {
            
            if ([[[userArray objectAtIndex:i] objectForKey:@"password"] isEqualToString:passwordText.text]) {
                return YES;
            }
        }
    }
    
    return NO;
}

-(void)performLogin
{
    if([[self.generalService doTrimString: emailText.text] isEqualToString:@""]){
        [self.generalService showErrorMessage:@"Please Enter Username"];
        [emailText becomeFirstResponder];
    }
    else if([[self.generalService doTrimString: passwordText.text] isEqualToString:@""]){
        [self.generalService showErrorMessage:@"Please Enter Password"];
        [passwordText becomeFirstResponder];
    }
    else{
        
        //If email and password is sam...
    
        if ([self loginAuthentication])
        {
//            [self.generalService showSuccessMessage:@"Login Successful!!"];
            
//            [self saveLoginDetails];
            
            //MainViewController *mainViewController = [[MainViewController alloc] init];
        
            //UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:mainViewController];
            //[self.view.window setRootViewController: nav];
            NewsProviderList *newProviderList = [[NewsProviderList alloc] initWithNibName:@"NewsProviderList" bundle:nil];
            AppDelegate *appDel = [[UIApplication sharedApplication] delegate];
//            appDel.navigation = [[UINavigationController alloc] initWithRootViewController:newProviderList];
            
            UINavigationController *navControl = [[UINavigationController alloc] initWithRootViewController:newProviderList];
            appDel.window.rootViewController = navControl;
            
//            [self.navigationController pushViewController:newProviderList animated:YES];
        }
        else {
            [self.generalService showErrorMessage:@"Wrong email or password. Try again."];
        }
    }
}

-(void)loadLoginDetails
{
    
    GeneralService *commonService = [[GeneralService alloc] init];
    BOOL isExistsFile = [[NSFileManager defaultManager]fileExistsAtPath:[commonService getFilePath]];
    
    if (isExistsFile)
    {
        NSArray *loginDetails = [[NSArray alloc] initWithContentsOfFile:[commonService getFilePath]];
        
        if([[loginDetails objectAtIndex:0] isEqualToString: @"1"])
        {
            rememberStatus = true;
            userID = [loginDetails objectAtIndex:1];
            password = [loginDetails objectAtIndex:2];
        
            return;
        }
    }
    
    rememberStatus = false;
}

-(void)saveLoginDetails
{
    GeneralService *commonService = [[GeneralService alloc] init];
    
    if(rememberStatus)
    {
        
        //Save Flag = 1
        NSArray *userData = [[NSArray alloc]initWithObjects: @"1", emailText.text, passwordText.text, nil] ;
        [userData writeToFile:[commonService getFilePath] atomically:YES];
    }
    else {
        //Don't Save Flag = 0
        NSArray *userData = [[NSArray alloc]initWithObjects: @"0", nil] ;
        [userData writeToFile:[commonService getFilePath] atomically:YES];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == emailText)
    {
        [passwordText becomeFirstResponder];
    }
    else if (textField == passwordText){
        [self performLogin];
    }
    return YES;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IPAD) {
        return 620.f;
    }
    return 425.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IPAD) {
        LoginCellPad *cell = (LoginCellPad *) [tableView dequeueReusableCellWithIdentifier:@"LoginCellPad"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LoginCellPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        
        /****************** Image LeftView ******************/
        
        UIImageView *imgEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgEmail.image = [UIImage imageNamed:@"user"];
        imgEmail.alpha = 0.6;
        imgEmail.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.emailText.leftViewMode = UITextFieldViewModeAlways;
        cell.emailText.delegate = self;
        cell.emailText.leftView = imgEmail;
        
        
        UIImageView *imgPass = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgPass.image = [UIImage imageNamed:@"password"];
        imgPass.alpha = 0.6;
        imgPass.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.passwordText.leftViewMode = UITextFieldViewModeAlways;
        cell.passwordText.delegate = self;
        cell.passwordText.leftView = imgPass;
        

        /*******************************************************/
        
        [cell.passwordText setSecureTextEntry:YES];
        cell.emailText.returnKeyType = UIReturnKeyNext;
        cell.passwordText.returnKeyType = UIReturnKeyGo;
        
        if(rememberStatus)
        {
            cell.emailText.text = userID;
            cell.passwordText.text = password;
        }
        
        emailText = cell.emailText;
        passwordText = cell.passwordText;
        
        //Action performed when login buttons are pressed
        [cell.loginButton addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        //Action performed when login buttons are pressed
        [cell.registrationButton addTarget:self action:@selector(registrationButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
    else {
        LoginTableViewCell *cell = (LoginTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"LoginTableViewCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LoginTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        
        /****************** Image LeftView ******************/
        
        UIImageView *imgEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgEmail.image = [UIImage imageNamed:@"user"];
        imgEmail.alpha = 0.6;
        imgEmail.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.emailText.leftViewMode = UITextFieldViewModeAlways;
        cell.emailText.delegate = self;
        cell.emailText.leftView = imgEmail;
        
        
        UIImageView *imgPass = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgPass.image = [UIImage imageNamed:@"password"];
        imgPass.alpha = 0.6;
        imgPass.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.passwordText.leftViewMode = UITextFieldViewModeAlways;
        cell.passwordText.delegate = self;
        cell.passwordText.leftView = imgPass;
        
        //    cell.emailText.layer.cornerRadius = 17.f;
        //    cell.passwordText.layer.cornerRadius = 17.f;
        //    cell.loginButton.layer.cornerRadius = 17.f;
        //    cell.registrationButton.layer.cornerRadius = 17.f;
        
        /*******************************************************/
        
        [cell.passwordText setSecureTextEntry:YES];
        cell.emailText.returnKeyType = UIReturnKeyNext;
        cell.passwordText.returnKeyType = UIReturnKeyGo;
        
        if(rememberStatus)
        {
            cell.emailText.text = userID;
            cell.passwordText.text = password;
        }
        
        emailText = cell.emailText;
        passwordText = cell.passwordText;
        
        //Action performed when login buttons are pressed
        [cell.loginButton addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        //Action performed when login buttons are pressed
        [cell.registrationButton addTarget:self action:@selector(registrationButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
}

@end
