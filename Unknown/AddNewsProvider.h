//
//  AddNewsProvider.h
//
//  Created by RASHAD on 5/30/15.
//

#import <UIKit/UIKit.h>

@interface AddNewsProvider : UITableViewController<UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) NSString *name;
@property (weak, nonatomic) NSString *url;

@property int rowNumber;

@end
