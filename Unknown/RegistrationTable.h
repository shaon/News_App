//  Created by RASHAD on 5/30/15.
//

#import <UIKit/UIKit.h>
#import "GeneralService.h"

@interface RegistrationTable : UITableViewController <UITextFieldDelegate>

@property(nonatomic,strong) GeneralService *generalService;

@end
