//
//  LinkDetails.m
//
//  Created by RASHAD on 5/30/15.
//

#import "LinkDetails.h"

@interface LinkDetails ()

@end

@implementation LinkDetails

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSURL *url = [NSURL URLWithString:[_urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:70/255.f green:130/255.f blue:47/255.f alpha:1];
    [self.navigationItem.backBarButtonItem setTitle:nil];
    self.navigationItem.backBarButtonItem.title = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
