//
//  AddLinksPhone.h
//
//  Created by RASHAD on 5/30/15.
//

#import <UIKit/UIKit.h>

@interface AddLinksPhone : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *links;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@end
