//  Created by RASHAD on 5/30/15.
//

#import "GeneralService.h"
#import "AppDelegate.h"

@implementation GeneralService

-(void) showErrorMessage:(NSString*)message{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                                 initWithTitle:nil
                                 message:message
                                 delegate:nil
                                 cancelButtonTitle:@"Okay"
                                 otherButtonTitles:nil];
    [errorAlert show];
}

-(void)showLoadingMessage{
    UIAlertView *loadingMessage = [[UIAlertView alloc]
                                 initWithTitle:@"Loading.....!"
                                 message:@"\n\n"
                                 delegate:nil
                                 cancelButtonTitle:@"cancel"
                                 otherButtonTitles:nil];
    [loadingMessage show];

}

-(void)showSuccessMessage:(NSString*)message{
    UIAlertView *successAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Success!"
                                 message:message
                                 delegate:nil
                                 cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    [successAlert show];
}

-(void)showSuccessMessage:(NSString*)message withTitle:(NSString*)title{
    UIAlertView *successAlert = [[UIAlertView alloc]
                                 initWithTitle:title
                                 message:message
                                 delegate:nil
                                 cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    [successAlert show];
}

-(UIActivityIndicatorView *)loading:(UIView*) UI{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator setFrame:UI.frame];
    [indicator setCenter: UI.center];
    [indicator setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.50]];
    [indicator setHidesWhenStopped:YES];
    
    [UI addSubview:indicator];
    return indicator;
}


-(NSString*)encodeAsString:(id)content{
    return [NSString stringWithFormat:@"%@",content];
}

-(NSString*)isNullLabel:(NSString*)name{   
    if ([name isEqualToString:@"<null>"]) {
        return @"N/A";
    }else{
        return name;
    }
}


-(NSString*)doTrimString:(NSString*)string{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString*)doTrimEmptyLines:(NSString*)string{
    
    for(int i=0; i<string.length; i++) {
        if([string characterAtIndex:i] != '\n')
            return string;
    }
    
    return @"";
}


-(NSString*)httpPrefic:(NSString*)url{    
    NSString *prefix = @"http://";
    url = [self doTrimString:url];    
    if ([url isEqualToString:@""]){
               return url;
    }else{        
        NSRange match;
        match = [url rangeOfString: prefix];
        if (match.length) {
            return url;
        }else{
            return [prefix stringByAppendingString:url];
        }
    }
}

-(NSString*)getFormatedDate:(NSString*)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *newDate= [dateFormat dateFromString:date];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    return [dateFormat stringFromDate:newDate];
}

-(NSString*) getFilePath
{
    NSArray *arrayPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[arrayPath objectAtIndex:0] stringByAppendingString:@"LoginCredential.plist"];

}

@end
