//  Created by RASHAD on 5/30/15.
//

#import "RegistrationTable.h"
#import "AppDelegate.h"
#import "RegistrationTableCell.h"
#import "RegistrationTableCellPad.h"

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface RegistrationTable ()
{
    UITextField *nameText;
    UITextField *ageText;
    UITextField *emailText;
    UITextField *passwordText;
    
    NSString *userID, *password;
    
}

@end

@implementation RegistrationTable

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden:YES];
    
//    self.tableView.backgroundColor = [UIColor colorWithRed:180/255.f green:195/255.f blue:110/255.f alpha:1];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    //[self setComponent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController removeFromParentViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboard
{
    [self.view endEditing:YES];
}


- (void)registrationButtonPressed:(id)sender
{
    self.generalService = [[GeneralService alloc] init];
    [self dismissKeyboard];
    
    [self performRegistration];
}

-(void)cancelButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)performRegistration
{
    if([[self.generalService doTrimString: nameText.text] isEqualToString:@""]){
        [self.generalService showErrorMessage:@"Please Enter Your Full Name"];
        [nameText becomeFirstResponder];
    }
    else if([[self.generalService doTrimString: ageText.text] isEqualToString:@""]){
        [self.generalService showErrorMessage:@"Please Enter Age"];
        [ageText becomeFirstResponder];
    }
    else if([[self.generalService doTrimString: emailText.text] isEqualToString:@""]){
        [self.generalService showErrorMessage:@"Please Enter Username"];
        [emailText becomeFirstResponder];
    }
    else if([[self.generalService doTrimString: passwordText.text] isEqualToString:@""]){
        [self.generalService showErrorMessage:@"Please Enter Password"];
        [passwordText becomeFirstResponder];
    }
    else{
        
        if ([self addUser])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Registration Successful" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(BOOL) addUser {
    NSMutableDictionary *tempUser = [[NSMutableDictionary alloc] init];
    [tempUser setObject:nameText.text forKey:@"name"];
    [tempUser setObject:ageText.text  forKey:@"age"];
    [tempUser setObject:emailText.text forKey:@"username"];
    [tempUser setObject:passwordText.text forKey:@"password"];
    NSMutableArray *userArray = [[NSMutableArray alloc] init];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if ([userDefault objectForKey:@"users"] != NULL) {
        userArray = [[userDefault objectForKey:@"users"] mutableCopy];
    }
    for (int i = 0 ; i < userArray.count; i++) {
        if ([[[userArray objectAtIndex:i] objectForKey:@"username"] isEqualToString:emailText.text]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Username already taken." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
    }
    
    [userArray addObject:tempUser];
    
    [userDefault setObject:userArray forKey:@"users"];
    
    [userDefault synchronize];
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == nameText)
    {
        [ageText becomeFirstResponder];
    }
    else if(textField == ageText)
    {
        [emailText becomeFirstResponder];
    }
    else if(textField == emailText)
    {
        [passwordText becomeFirstResponder];
    }
    else if (textField == passwordText){
        [self performRegistration];
    }
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (IPAD) {
        return 620.f;
    }
    return 425.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IPAD) {
        RegistrationTableCellPad *cell = (RegistrationTableCellPad *) [tableView dequeueReusableCellWithIdentifier:@"RegistrationTableCellPad"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RegistrationTableCellPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        
        /****************** Image LeftView ******************/
        
        UIImageView *imgName = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgName.image = [UIImage imageNamed:@"name"];
        imgName.alpha = 0.6;
        imgName.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.nameText.leftViewMode = UITextFieldViewModeAlways;
        cell.nameText.delegate = self;
        cell.nameText.leftView = imgName;
        
        
        UIImageView *imgAge = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgAge.image = [UIImage imageNamed:@"age"];
        imgAge.alpha = 0.6;
        imgAge.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.ageText.leftViewMode = UITextFieldViewModeAlways;
        cell.ageText.delegate = self;
        cell.ageText.leftView = imgAge;
        
        
        UIImageView *imgEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgEmail.image = [UIImage imageNamed:@"user"];
        imgEmail.alpha = 0.6;
        imgEmail.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.emailText.leftViewMode = UITextFieldViewModeAlways;
        cell.emailText.delegate = self;
        cell.emailText.leftView = imgEmail;
        
        
        UIImageView *imgPass = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgPass.image = [UIImage imageNamed:@"password"];
        imgPass.alpha = 0.6;
        imgPass.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.passwordText.leftViewMode = UITextFieldViewModeAlways;
        cell.passwordText.delegate = self;
        cell.passwordText.leftView = imgPass;
        
        [cell.passwordText setSecureTextEntry:YES];
        
        cell.nameText.returnKeyType = UIReturnKeyNext;
        cell.ageText.returnKeyType = UIReturnKeyNext;
        cell.emailText.returnKeyType = UIReturnKeyNext;
        cell.passwordText.returnKeyType = UIReturnKeyGo;
        
        nameText = cell.nameText;
        ageText = cell.ageText;
        emailText = cell.emailText;
        passwordText = cell.passwordText;
        
        //Action performed when Sign Up buttons are pressed
        [cell.registrationButton addTarget:self action:@selector(registrationButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        //Action performed when Cancel buttons are pressed
        [cell.cancelButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;

    }
    else {
        RegistrationTableCell *cell = (RegistrationTableCell *) [tableView dequeueReusableCellWithIdentifier:@"RegistrationTableCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RegistrationTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        // Configure the cell...
        
        /****************** Image LeftView ******************/
        
        UIImageView *imgName = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgName.image = [UIImage imageNamed:@"name"];
        imgName.alpha = 0.6;
        imgName.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.nameText.leftViewMode = UITextFieldViewModeAlways;
        cell.nameText.delegate = self;
        cell.nameText.leftView = imgName;
        
        
        UIImageView *imgAge = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgAge.image = [UIImage imageNamed:@"age"];
        imgAge.alpha = 0.6;
        imgAge.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.ageText.leftViewMode = UITextFieldViewModeAlways;
        cell.ageText.delegate = self;
        cell.ageText.leftView = imgAge;
        
        
        UIImageView *imgEmail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgEmail.image = [UIImage imageNamed:@"user"];
        imgEmail.alpha = 0.6;
        imgEmail.contentMode = UIViewContentModeScaleAspectFit;
        
        
        cell.emailText.leftViewMode = UITextFieldViewModeAlways;
        cell.emailText.delegate = self;
        cell.emailText.leftView = imgEmail;
        
        
        UIImageView *imgPass = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        imgPass.image = [UIImage imageNamed:@"password"];
        imgPass.alpha = 0.6;
        imgPass.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.passwordText.leftViewMode = UITextFieldViewModeAlways;
        cell.passwordText.delegate = self;
        cell.passwordText.leftView = imgPass;
        
        [cell.passwordText setSecureTextEntry:YES];
        
        cell.nameText.returnKeyType = UIReturnKeyNext;
        cell.ageText.returnKeyType = UIReturnKeyNext;
        cell.emailText.returnKeyType = UIReturnKeyNext;
        cell.passwordText.returnKeyType = UIReturnKeyGo;
        
        nameText = cell.nameText;
        ageText = cell.ageText;
        emailText = cell.emailText;
        passwordText = cell.passwordText;
        
        //Action performed when Sign Up buttons are pressed
        [cell.registrationButton addTarget:self action:@selector(registrationButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        //Action performed when Cancel buttons are pressed
        [cell.cancelButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;
    }
}

@end
