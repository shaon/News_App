//
//  LinkDetails.h
//
//  Created by RASHAD on 5/30/15.
//

#import <UIKit/UIKit.h>

@interface LinkDetails : UIViewController

@property (nonatomic, strong) NSString *urlString;

@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end
