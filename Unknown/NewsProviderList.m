//
//  NewsProviderList.m
//
//  Created by RASHAD on 5/30/15.
//

#import "NewsProviderList.h"
#import "AddNewsProvider.h"
#import "NewsListingCell.h"
#import "LinkListController.h"
#import "LoginTableView.h"

@interface NewsProviderList () {
    NSMutableArray *providerList;
}

@end

@implementation NewsProviderList

- (void)viewDidLoad
{
    [super viewDidLoad];
    providerList = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed: @"add"] style:UIBarButtonItemStylePlain target:self action:@selector(addNewsProvider)];
    
    button.tintColor = [UIColor colorWithRed:70/255.f green:130/255.f blue:47/255.f alpha:1];
    
    UIBarButtonItem *logoutButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed: @"logout"] style:UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    logoutButton.tintColor = [UIColor colorWithRed:70/255.f green:130/255.f blue:47/255.f alpha:1];
    
    self.navigationItem.title = @"Home";
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.rightBarButtonItem = button;
    self.navigationItem.leftBarButtonItem = logoutButton;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)logout
{
    LoginTableView *login = [[LoginTableView alloc] init];
    
    [self.navigationController pushViewController:login animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addNewsProvider {
    AddNewsProvider *addNewsProvider = [[AddNewsProvider alloc] initWithNibName:@"AddNewsProvider" bundle:nil];
    addNewsProvider.rowNumber = -1;
    [self.navigationController pushViewController:addNewsProvider animated:YES];
}

-(void)loadData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults objectForKey:@"provider"] != NULL) {
        providerList = [[userDefaults objectForKey:@"provider"] mutableCopy];
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return providerList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsListingCell *cell = (NewsListingCell *) [tableView dequeueReusableCellWithIdentifier:@"NewsListingCell"];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsListingCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.name.text = [[providerList objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    cell.link.text = [[providerList objectAtIndex:indexPath.row] objectForKey:@"link"];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75.f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please select" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"View",@"Edit",@"Delete",@"Hide", nil];
    alert.tag = indexPath.row;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"View"]) {
        LinkListController *linkList = [[LinkListController alloc] initWithNibName:@"LinkListController" bundle:nil];
        linkList.urlString = [[providerList objectAtIndex:alertView.tag] objectForKey:@"link"];
        linkList.name = [[providerList objectAtIndex:alertView.tag] objectForKey:@"name"];
        [self.navigationController pushViewController:linkList animated:YES];
    }
    else if([title isEqualToString:@"Edit"]) {
        
        AddNewsProvider *addNewsProvider = [[AddNewsProvider alloc] initWithNibName:@"AddNewsProvider" bundle:nil];
        addNewsProvider.name = [[providerList objectAtIndex:alertView.tag] objectForKey:@"name"];
        addNewsProvider.url = [[providerList objectAtIndex:alertView.tag] objectForKey:@"link"];
        addNewsProvider.rowNumber = (int)alertView.tag;
        [self.navigationController pushViewController:addNewsProvider animated:YES];
    }
    else if([title isEqualToString:@"Delete"]) {
        [self deleteEntry:alertView.tag];
    }
}

-(void) deleteEntry : (NSInteger) row{
    [providerList removeObjectAtIndex:row];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:providerList forKey:@"provider"];
    [userDefaults synchronize];
    [self.tableView reloadData];
}

@end
