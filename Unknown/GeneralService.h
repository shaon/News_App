//  Created by RASHAD on 5/30/15.
//

#import <Foundation/Foundation.h>

@interface GeneralService : NSObject

-(void) showErrorMessage:(NSString*)message;
-(void)showLoadingMessage;
-(void)showSuccessMessage:(NSString*)message;
-(void)showSuccessMessage:(NSString*)message withTitle:(NSString*)title;
-(NSString*)encodeAsString:(NSNumber*)numbet;
-(NSString*)isNullLabel:(NSString*)name;
-(NSString*)doTrimString:(NSString*)string;
-(NSString*)doTrimEmptyLines:(NSString*)string;
-(NSString*)httpPrefic:(NSString*)url;
-(NSString*)getFormatedDate:(NSString*)date;
-(NSString*) getFilePath;

@end
